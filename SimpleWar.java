public class SimpleWar{
	public static void main(String[] args) {
		int player1Points = 0;
		int player2Points = 0;
		
		Deck pile = new Deck();
		pile.shuffle();
		while (pile.length() > 1) {
			pile.shuffle();
			Card card1 = pile.drawTopCard();
			Card card2 = pile.drawTopCard();
			double card1Score = card1.calculateScore();
			double card2Score = card2.calculateScore();
			
			System.out.println("Player1's card: " + card1);
			System.out.println("Card1's value: " + card1Score);
			System.out.println("Player2's card: " + card2);		
			System.out.println("Card2's value: " + card2Score);
			System.out.println("Player1's points: " + player1Points);
			System.out.println("Player2's points: " + player2Points);
			if(card1Score > card2Score){
				player1Points +=1;
			}else{
				player2Points +=1;
			}
			System.out.println("Player1's points: " + player1Points);
			System.out.println("Player2's points: " + player2Points);
		}
        if (player1Points > player2Points) {
            System.out.println("Player1 wins");
        }
        System.out.println("Player2 wins");
	}
}
