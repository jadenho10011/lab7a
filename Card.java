public class Card{
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return suit;
	}
	
	public String getValue(){
		return value;
	}
	
	public String toString(){
		return value + " of " + suit;
	}
	
	public double calculateScore(){
		double completeScore = 0.0;
		double suitPoints = 0.0;
		double valuePoints = 0.0;
		
		if(this.value.equals("Ace")){
			suitPoints += 1.0;
		} else if(this.value.equals("Two")){
			suitPoints += 2.0;
		}else if(this.value.equals("Three")){
			suitPoints += 3.0;
		}else if(this.value.equals("Four")){
			suitPoints += 4.0;
		}else if(this.value.equals("Five")){
			suitPoints += 5.0;
		}else if(this.value.equals("Six")){
			suitPoints += 6.0;
		}else if(this.value.equals("Seven")){
			suitPoints += 7.0;
		}else if(this.value.equals("Eight")){
			suitPoints += 8.0;
		}else if(this.value.equals("Nine")){
			suitPoints += 9.0;
		}else if(this.value.equals("Ten")){
			suitPoints += 10.0;
		}else if(this.value.equals("Jack")){
			suitPoints += 11.0;
		}else if(this.value.equals("Queen")){
			suitPoints += 12.0;
		}else if(this.value.equals("King")){
			suitPoints += 13.0;
		}
		if(this.suit.equals("Hearts")){
			valuePoints += 0.1;
		}else if(this.suit.equals("Spades")){
			valuePoints += 0.2;
		}else if(this.suit.equals("Diamonds")){
			valuePoints += 0.3;
		}else if(this.suit.equals("Clubs")){
			valuePoints += 0.4;
		}
		completeScore += valuePoints + suitPoints;
		return completeScore;
	}
}